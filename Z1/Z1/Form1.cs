﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z1
{
    public partial class Form1 : Form
    {
        double br1, br2;

        public Form1()
        {
            InitializeComponent();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1))
            {
                textBox1.Text = Math.Sin(br1).ToString();
            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1))
            {
                textBox1.Text = Math.Cos(br1).ToString();
            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1))
            {
                textBox1.Text = Math.Tan(br1).ToString();
            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1))
            {
                textBox1.Text = (1 / Math.Tan(br1)).ToString();
            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1) && double.TryParse(textBox2.Text, out br2))
            {
                textBox1.Text = (br1 + br2).ToString();

            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1) && double.TryParse(textBox2.Text, out br2))
            {
                textBox1.Text = (br1 - br2).ToString();

            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1) && double.TryParse(textBox2.Text, out br2))
            {
                textBox1.Text = (br1 * br2).ToString();

            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1) && double.TryParse(textBox2.Text, out br2))
            {
                textBox1.Text = (br1 / br2).ToString();

            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out br1))
            {
                textBox1.Text = Math.Sqrt(br1).ToString();
            }
            else
            {
                MessageBox.Show("Potrebno je unjeti broj", "Pogreška");
                textBox1.Clear();
            }
        }
    }
}
